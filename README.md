## Description

Resolvi usar o nest.js como framework para desevolver o teste por ser bem completo, ter varias features como injeção de dependencia, e também possuir uma documentação bem detalhada e completada.

Infelizmente faltou desenvolver a parte de salvar as interações no elasticsearch, mas acho que pelo o que eu fiz ja dar para vocês terem ideia de como eu escrevo codigo.

A API pode ser rodada pelo docker ou direto no terminal. 

URL base: http://localhost:8080

## Endpoints

### Get websites

Endpoint referente ao primeiro topico do teste: 1. Os websites de todos os usuários.

`/users/websites`

### Get users

Endpoint referente ao segundo topico do teste: O Nome, email e a empresa em que trabalha (em ordem alfabética).

`/users`

### Get suites users

Endpoint referente ao terceiro topico do teste: Mostrar todos os usuários que no endereço contem a palavra ```suite```

`/users/suites`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test


