import * as faker from 'faker';

import { JSONPlaceHolderUser } from 'src/interfaces';

export const jsonPlaceHolderUser: JSONPlaceHolderUser = {
  id: faker.random.number(),
  name: faker.name.firstName(),
  username: faker.random.word(),
  email: faker.internet.email(),
  address: {
    street: faker.address.streetName(),
    suite: faker.address.streetAddress(),
    city: faker.address.city(),
    zipcode: faker.address.zipCode(),
    geo: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
  },
  phone: faker.phone.phoneNumber(),
  website: faker.internet.url(),
  company: {
    catchPhrase: faker.random.words(),
    name: faker.random.word(),
    bs: faker.random.words(),
  },
};

export const jsonPlaceHolderSuiteUser: JSONPlaceHolderUser = {
  id: faker.random.number(),
  name: faker.name.firstName(),
  username: faker.random.word(),
  email: faker.internet.email(),
  address: {
    street: faker.address.streetName(),
    suite: 'Suite 385',
    city: faker.address.city(),
    zipcode: faker.address.zipCode(),
    geo: {
      lat: faker.address.latitude(),
      lng: faker.address.longitude(),
    },
  },
  phone: faker.phone.phoneNumber(),
  website: faker.internet.url(),
  company: {
    catchPhrase: faker.random.words(),
    name: faker.random.word(),
    bs: faker.random.words(),
  },
};

export const jsonPlaceHolderResponseFixture = [
  jsonPlaceHolderSuiteUser,
  jsonPlaceHolderUser,
];
