import { jsonPlaceHolderSuiteUser, jsonPlaceHolderUser } from './json-place-holder-response';
import { User } from '../../src/domain/entities/user';

export const user = new User(
  jsonPlaceHolderSuiteUser.id, jsonPlaceHolderSuiteUser.name,
  jsonPlaceHolderSuiteUser.username, jsonPlaceHolderSuiteUser.email,
  jsonPlaceHolderSuiteUser.address, jsonPlaceHolderSuiteUser.phone,
  jsonPlaceHolderSuiteUser.website, jsonPlaceHolderSuiteUser.company,
);

export const anotherUser = new User(
  jsonPlaceHolderUser.id, jsonPlaceHolderUser.name,
  jsonPlaceHolderUser.username, jsonPlaceHolderUser.email,
  jsonPlaceHolderUser.address, jsonPlaceHolderUser.phone,
  jsonPlaceHolderUser.website, jsonPlaceHolderUser.company,
);

export const userAlbert = new User(
  jsonPlaceHolderSuiteUser.id, 'Albert',
  jsonPlaceHolderSuiteUser.username, jsonPlaceHolderSuiteUser.email,
  jsonPlaceHolderSuiteUser.address, jsonPlaceHolderSuiteUser.phone,
  jsonPlaceHolderSuiteUser.website, jsonPlaceHolderSuiteUser.company,
);

export const userBaker = new User(
  jsonPlaceHolderSuiteUser.id, 'Baker',
  jsonPlaceHolderSuiteUser.username, jsonPlaceHolderSuiteUser.email,
  jsonPlaceHolderSuiteUser.address, jsonPlaceHolderSuiteUser.phone,
  jsonPlaceHolderSuiteUser.website, jsonPlaceHolderSuiteUser.company,
);

export const userClementina = new User(
  jsonPlaceHolderSuiteUser.id, 'Clementina',
  jsonPlaceHolderSuiteUser.username, jsonPlaceHolderSuiteUser.email,
  jsonPlaceHolderSuiteUser.address, jsonPlaceHolderSuiteUser.phone,
  jsonPlaceHolderSuiteUser.website, jsonPlaceHolderSuiteUser.company,
);

export const userClementine = new User(
  jsonPlaceHolderSuiteUser.id, 'clementine',
  jsonPlaceHolderSuiteUser.username, jsonPlaceHolderSuiteUser.email,
  jsonPlaceHolderSuiteUser.address, jsonPlaceHolderSuiteUser.phone,
  jsonPlaceHolderSuiteUser.website, jsonPlaceHolderSuiteUser.company,
);

export const specificUsers = [
  userClementine,
  userBaker,
  userAlbert,
  userClementina,
];

export const users = [
  anotherUser,
  user,
];
