import { UserService } from '../../../src/domain/services';
import * as fixtures from '../../fixtures';

describe('UserService', () => {
  const userService: UserService = new UserService();

  describe('createUsersFromJsonPlaceHolderResponse()', () => {
    it('should return array of users', () => {
      const users = userService.createUsersFromJsonPlaceHolderResponse(fixtures.jsonPlaceHolderResponseFixture);

      const expectedResult = [
        fixtures.user,
        fixtures.anotherUser,
      ];

      expect(users).toEqual(expectedResult);
    });
  });

  describe('getUsersWebsites()', () => {
    it('should return users website and username list', () => {
      const websites = userService.getUsersWebsites(fixtures.users);

      const expectedResult = [
        {
          username: fixtures.anotherUser.username,
          website: fixtures.anotherUser.website,
        },
        {
          username: fixtures.user.username,
          website: fixtures.user.website,
        },
      ];

      expect(websites).toEqual(expectedResult);
    });
  });

  describe('orderAlphabetically()', () => {
    it('should return users alphabetically ordered', () => {
      const orderedUsers = userService.orderAlphabetically(fixtures.specificUsers);

      const expectedResult = [
        fixtures.userAlbert,
        fixtures.userBaker,
        fixtures.userClementina,
        fixtures.userClementine,
      ];

      expect(orderedUsers).toEqual(expectedResult);
    });
  });

  describe('getAddressSuitesUsers()', () => {
    it('should return only users that contains suite word in address', () => {
      const suiteAddressUsers = userService.getAddressSuitesUsers(fixtures.users);

      const expectedResult = [fixtures.user];

      expect(suiteAddressUsers).toEqual(expectedResult);
    });
  });
});
