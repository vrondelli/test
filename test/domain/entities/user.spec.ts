import { User } from '../../../src/domain/entities/user';
import * as fixtures from '../../fixtures';

describe('User entitie', () => {
  describe('addressContainsSuiteWord()', () => {
    it('should return true when address contains suite word', () => {
      const user = new User(
        fixtures.jsonPlaceHolderSuiteUser.id, fixtures.jsonPlaceHolderSuiteUser.name,
        fixtures.jsonPlaceHolderSuiteUser.username, fixtures.jsonPlaceHolderSuiteUser.email,
        fixtures.jsonPlaceHolderSuiteUser.address, fixtures.jsonPlaceHolderSuiteUser.phone,
        fixtures.jsonPlaceHolderSuiteUser.website, fixtures.jsonPlaceHolderSuiteUser.company,
      );

      expect(user.addressContainsSuiteWord()).toBe(true);
    });

    it('should return false when address does not contain suite word', () => {
      const user = new User(
        fixtures.jsonPlaceHolderUser.id, fixtures.jsonPlaceHolderUser.name,
        fixtures.jsonPlaceHolderUser.username, fixtures.jsonPlaceHolderUser.email,
        fixtures.jsonPlaceHolderUser.address, fixtures.jsonPlaceHolderUser.phone,
        fixtures.jsonPlaceHolderUser.website, fixtures.jsonPlaceHolderUser.company,
      );

      expect(user.addressContainsSuiteWord()).toBe(false);
    });
  });
});
