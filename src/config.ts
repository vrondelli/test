export default {
  servicePort: process.env.SERVICE_PORT || 8080,
  jsonPlaceHolderServiceUrl: process.env.JSON_SERVICE_URL || 'https://jsonplaceholder.typicode.com',
};
