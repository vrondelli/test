export const wordInString = (str: string, word: string) => {
  return new RegExp( '\\b' + word + '\\b', 'i').test(str);
};
