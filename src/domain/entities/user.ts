import { wordInString } from '../../utils';

export class User {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public email: string,
    public address: {
      street: string,
      suite: string,
      city: string,
      zipcode: string,
      geo: {
        lat: string,
        lng: string,
      };
    },
    public phone: string,
    public website: string,
    public company: {
      name: string,
      catchPhrase: string,
      bs: string,
    },
  ) {}

  public addressContainsSuiteWord() {
    return Object.values(this.address).filter(value => {
      if (typeof value === 'string') {
        return wordInString(value, 'suite');
      }
    }).length > 0;
  }
}
