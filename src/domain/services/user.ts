import { Injectable } from '@nestjs/common';
import { JSONPlaceHolderUser } from '../../interfaces';
import { User } from '../entities/user';

@Injectable()
export class UserService {
  public createUsersFromJsonPlaceHolderResponse(response: JSONPlaceHolderUser[]): User[] {
    return response.map(r => {
      return new User(r.id, r.name, r.username, r.email, r.address, r.phone, r.website, r.company);
    });
  }

  public getUsersWebsites(users: User[]) {
    return users.map(user => {
      return {
        username: user.username,
        website: user.website,
      };
    });
  }

  public orderAlphabetically(users: User[]) {
    return users.sort((a, b) => {
      const aName = a.name.toUpperCase();
      const bName = b.name.toUpperCase();

      return aName < bName ? -1 : aName > bName ? 1 : 0;
    });
  }

  public getAddressSuitesUsers(users: User[]) {
    return users.filter(user => {
      return user.addressContainsSuiteWord();
    });
  }
}
