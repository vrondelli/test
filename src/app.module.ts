import { Module } from '@nestjs/common';
import { JSONPlaceHolderService } from './app/services/json-place-holder';
import { UserService } from './domain/services';
import { UserController } from './app/controllers';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [JSONPlaceHolderService, UserService],
})
export class AppModule {}
