import * as request from 'request-promise-native';
import { Injectable } from '@nestjs/common';
import * as Joi from '@hapi/joi';

import { JSONPlaceHolderUser } from '../../interfaces/json-place-holder-response';
import config from '../../config';

@Injectable()
export class JSONPlaceHolderService {
  private readonly schema = Joi.array().items(Joi.object().keys({
    id: Joi.number().required(),
    name: Joi.string().required(),
    username: Joi.string().required(),
    email: Joi.string().required(),
    address: Joi.object().keys({
      street: Joi.string().required(),
      suite: Joi.string().required(),
      city: Joi.string().required(),
      zipcode: Joi.string().required(),
      geo: Joi.object().keys({
        lat: Joi.string().required(),
        lng: Joi.string().required(),
      }),
    }),
    phone: Joi.string().required(),
    website: Joi.string().required(),
    company: Joi.object().keys({
      name: Joi.string().required(),
      catchPhrase: Joi.string().required(),
      bs: Joi.string().required(),
    }),
  }));

  public async getUsers(): Promise<JSONPlaceHolderUser[]> {
    return request.get(
     `${config.jsonPlaceHolderServiceUrl}/users`,
     { json: true },
    ).then(response => {
      const { error } = Joi.validate(response, this.schema);

      if (error) {
        throw new Error(error.message);
      }

      return response as JSONPlaceHolderUser[];
    });
  }
}
