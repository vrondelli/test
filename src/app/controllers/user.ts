import { Controller, Get, InternalServerErrorException } from '@nestjs/common';
import { JSONPlaceHolderService } from '../services';
import { UserService } from '../../domain/services';
import { User } from 'src/domain/entities/user';

@Controller('users')
export class UserController {
  constructor(
    private readonly jsonPlaceHolderService: JSONPlaceHolderService,
    private readonly userService: UserService,
  ) {}

  @Get('websites')
  public async getUsersWebsites() {
    let users: User[];

    try {
      users = await this.getUsersFromJsonPlaceHolder();
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }

    return this.userService.getUsersWebsites(users);
  }

  @Get()
  public async getUsers() {
    let users: User[];

    try {
      users = await this.getUsersFromJsonPlaceHolder();
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }

    const orderedUsersInfo = [];

    this.userService.orderAlphabetically(users).forEach(user => {
      orderedUsersInfo.push({
        username: user.username,
        name: user.name,
        email: user.email,
        company: user.company,
      });
    });

    return orderedUsersInfo;
  }

  @Get('suites')
  public async getSuitesUsers() {
    let users: User[];

    try {
      users = await this.getUsersFromJsonPlaceHolder();
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }

    return this.userService.getAddressSuitesUsers(users);
  }

  private async getUsersFromJsonPlaceHolder() {
    const  jsonPlaceHolderResponse = await this.jsonPlaceHolderService.getUsers();

    return this.userService.createUsersFromJsonPlaceHolderResponse(jsonPlaceHolderResponse);
  }
}
